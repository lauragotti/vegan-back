/* 2023-07-26 11:25:52 [221 ms] */ 
-- Active: 1673947610856@@127.0.0.1@3306@vegan
DROP TABLE IF EXISTS likes;
DROP TABLE IF EXISTS picture;
DROP TABLE IF EXISTS post;
DROP TABLE IF EXISTS user; 

CREATE TABLE post(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    country VARCHAR(255),
    city VARCHAR(255),
    address VARCHAR(255),
    description VARCHAR(255),
    fullVegan BOOLEAN,
    visible BOOLEAN,
    website VARCHAR(255)
);

CREATE TABLE picture(
    id INT PRIMARY KEY AUTO_INCREMENT,
    src VARCHAR(255),
    idPost INT,
    Foreign Key (idPost) REFERENCES post(id)
);
CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL
);

CREATE TABLE likes(
    id INT PRIMARY KEY AUTO_INCREMENT,
    idPost INT,
    Foreign Key (idPost) REFERENCES post(id) ON DELETE CASCADE,
    idUser INT,
    Foreign Key (idUser) REFERENCES user(id) ON DELETE CASCADE
);

INSERT INTO post (name, country, city, address, description, fullVegan, visible, website)
VALUES
(
    'Hank Burger',
    'France',
    'Lyon',
    '5 Rue Pizay',
    'Hank Vegan Burger est la marque de burger 100% Vegan, 100% Plaisir. Restaurants à Paris, Lille et Lyon pour manger sur place, à emporter et en click and collect.',
    1,
    1,
    'https://www.hankrestaurant.com/'
),
(
    'Zoï',
    'France',
    'Lyon',
    '46 Montée de la Grande Côte',
    'Des pâtisseries 100% végétales, gourmandes et créatives. Et toujours 100% faites maison ! .',
    1,
    1,
    'www.zoi-kitchen.com'
);

INSERT INTO user (name, email, password, role) VALUES 
(
    'Laura',
    'laura@laura.com',
    '1234',
    'ROLE_USER'
),
(
    'Julie',
    'julie@julie.com',
    '1234',
    'ROLE_ADMIN'
);

INSERT INTO likes (idPost, idUser) VALUES
(
    1,
    1
),
(
    1,
    2
),
(
    2,
    1
)