<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/post')]
class PostController extends AbstractController
{
    private PostRepository $repo;
    public function __construct(PostRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all()
    {
        $posts = $this->repo->findAll();
        return $this->json($posts);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $post = $this->repo->findById($id);
        if (!$post) {
            throw new NotFoundHttpException();

        }
        return $this->json($post);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $post = $serializer->deserialize($request->getContent(), Post::class, 'json');
            $this->repo->persist($post);
            return $this->json($post, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id)
    {
        $post = $this->repo->findById($id);
        if (!$post) {
            throw new NotFoundHttpException();
        }
        $this->repo->delete($post);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route(methods: 'PUT')]
    public function put(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
      $post = $serializer->deserialize($request->getContent(), Post::class, 'json');
    

        if (!$post) {
            throw new NotFoundHttpException();
        }
        try {
          $this->repo->update($post);
          return $this->json($post, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/{id}/like', methods: 'POST')]
    public function toggleLike(int $id)
    {
        /**
         * @var User
         */
        $user = $this->getUser();
        // Toggle the like
        $liked = $this->repo->toggleLike($user->getId(), $id);

        return $this->json(['liked' => $liked]);
    }

    #[Route('/search/{query}', methods: 'GET')]
    public function search(string $query)
    {
        $matchingPosts = $this->repo->searchInCity($query);
        
        if (empty($matchingPosts)) {
            return $this->json(['message' => 'No results']);
        }

        return $this->json($matchingPosts);
    }



}