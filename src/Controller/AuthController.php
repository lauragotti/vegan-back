<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends AbstractController
{
  private UserRepository $repo;

  public function __construct(UserRepository $repo)
  {
    $this->repo = $repo;
  }

  #[Route('/api/user', methods: 'POST')]
  public function index(UserRepository $repo, Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher, ValidatorInterface $validator): JsonResponse
  {
    try {
      $user = $serializer->deserialize($request->getContent(), User::class, 'json');

    } catch (\Exception $e) {
      return $this->json('Invalid body', 400);
    }

    $errors = $validator->validate($user);
    if ($errors->count() > 0) {
      return $this->json(['errors' => $errors], 400);
    }

    if ($repo->findByEmail($user->getEmail())) {
      return $this->json('User Already exists', 400);
    }

    $hash = $hasher->hashPassword($user, $user->getPassword());
    $user->setPassword($hash);
    $user->setRole('ROLE_USER');

    $repo->persist($user);

    return $this->json($user, 201);

  }

  #[Route('/api/protected', methods: 'GET')]
  public function getLogged()
  {
    return $this->json($this->getUser());
  }

  #[Route('/api/protected/likes', methods: 'GET')]
  public function getLikes()
  {
    /**
     * @var User
     */
    $user = $this->getUser();
    $likes = $this->repo->findLikesByUserId($user->getId());
    return $this->json($likes);

  }

  #[Route('/api/protected', methods: 'DELETE')]
  public function deleteUser()
  {
    $user = $this->getUser();

    if (!$user) {
      return $this->json(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
    }

    try {
      $this->repo->delete($user);
      return $this->json(['message' => 'User deleted successfully']);
    } catch (\Exception $e) {
      return $this->json(['error' => 'Failed to delete user'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
  }

  #[Route('/api/protected', methods: 'PUT')]

  public function updateUser(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher)
  {
    try {
      /**
       * @var User
       */
      $user = $this->getUser();
      $requestData = json_decode($request->getContent(), true);
      if (isset($requestData['name'])) {
        $user->setName($requestData['name']);
      }
      if (isset($requestData['email'])) {
       
            $existingUser = $this->repo->findByEmail($requestData['email']);
            if ($existingUser && $existingUser->getId() !== $user->getId()) {
                return $this->json('Email already exists', Response::HTTP_BAD_REQUEST);
            }
            
        $user->setEmail($requestData['email']);
    }
      $this->repo->update($user);
      return $this->json($user, Response::HTTP_OK);

    } catch (ValidationFailedException $e) {
      return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
    } catch (NotEncodableValueException $e) {
      return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
    }
  }

  #[Route('/api/protected/password', methods: 'PUT')]
  public function updatePassword(Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher, ValidatorInterface $validator)
  {
    /**
     * @var User
     */
    $user = $this->getUser();
    $requestData = json_decode($request->getContent(), true);

    if (isset($requestData['oldPassword']) && isset($requestData['newPassword'])) {
      $oldPassword = $requestData['oldPassword'];
      $newPassword = $requestData['newPassword'];

      if ($hasher->isPasswordValid($user, $oldPassword)) {
        $user->setPassword($newPassword);
        $errors = $validator->validate($user);
        if ($errors->count() > 0) {
          return $this->json(['errors' => $errors], 400);
        }
        $hash = $hasher->hashPassword($user, $newPassword);
        $user->setPassword($hash);
        $this->repo->update($user);
        return $this->json('Password modified');
      } else {
        return $this->json('Invalid password', Response::HTTP_BAD_REQUEST);

      }
    }

  }
}