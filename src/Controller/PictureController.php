<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Repository\PictureRepository;
use App\Service\Uploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/picture')]
class PictureController extends AbstractController {
    private PictureRepository $repo;
    public function __construct(PictureRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all()
    {
        $pictures = $this->repo->findAll();
        return $this->json($pictures);
    }
    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $picture = $this->repo->findById($id);
        if (!$picture) {
            throw new NotFoundHttpException();
        }
        return $this->json($picture);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, Uploader $uploader)
    {
        try {
            $picture = $serializer->deserialize($request->getContent(), Picture::class, 'json');
            $picture->setSrc($uploader->upload($picture->getSrc()));
            $this->repo->persist($picture);
            return $this->json($picture, Response::HTTP_CREATED);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('invalid json', Response::HTTP_BAD_REQUEST);

        }

    }


    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id, Uploader $uploader)
    {
        $picture = $this->repo->findById($id);
        if (!$picture) {
            throw new NotFoundHttpException();
        }
        $uploader->delete($picture->getSrc());
        $this->repo->delete($picture);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
    
    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $picture = $this->repo->findById($id);
        if (!$picture) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), Picture::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);
            return $this->json($toUpdate);
        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }

}
