<?php

namespace App\Repository;

use App\Entity\Picture;
use PDO;

class PictureRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }
    public function sqlToPicture(array $line): Picture
    {
        return new Picture($line['src'], $line['idPost'], (int) $line['id']);
    }

    public function findAll(): array
    {
        $pictures = [];
        $statement = $this->connection->prepare('SELECT * FROM picture');
        $statement->execute();
        $results = $statement->fetchAll();

        foreach ($results as $item) {
            $pictures[] = $this->sqlToPicture($item);
        }

        return $pictures;
    }

    public function findById(int $id): ?Picture
    {
        $statement = $this->connection->prepare('SELECT * FROM picture WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();

        if ($result) {
            return $this->sqlToPicture($result);
        }

        return null;
    }

    public function persist(Picture $picture)
    {
        $statement = $this->connection->prepare('INSERT INTO picture (src, idPost) VALUES (:src, :idPost)');
        $statement->execute([
            'src' => $picture->getSrc(),
            'idPost' => $picture->getidPost(),
        ]);

        $picture->setId($this->connection->lastInsertId());
    }

    public function delete(Picture $picture)
    {
        $statement = $this->connection->prepare('DELETE FROM picture WHERE id=:id');
        $statement->bindValue('id', $picture->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function update(Picture $picture): void
    {
        $statement = $this->connection->prepare('UPDATE picture SET src=:src, idPost=:idPost WHERE id=:id');
        $statement->execute([
            'src' => $picture->getSrc(),
            'idPost' => $picture->getidPost(),
            'id' => $picture->getId(),
        ]);
    }

}