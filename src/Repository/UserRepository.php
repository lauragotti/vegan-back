<?php

namespace App\Repository;

use App\Entity\User;
use PDO;

class UserRepository
{
    private PDO $connection;

    public function __construct(private PostRepository $postrepo)
    {
        $this->connection = Connection::getConnection();

    }

    public function sqlToUser(array $line): User
    {
        return new User(
            $line['name'],
            $line['email'],
            $line['password'],
            $line['role'],
            (int) $line['id']
        );
    }

    public function findLikesByUserId(int $userId){
        $statement = $this->connection->prepare('SELECT p.* FROM likes l LEFT JOIN post p ON l.idPost = p.id WHERE l.idUser = :userId');
        $statement->bindValue('userId', $userId);
        $statement->execute();
        $results = $statement->fetchAll();
        $likes = [];
        foreach ($results as $item) {
            $likes[] = [
                'id' => $item['id'],
                'name' => $item['name'],
                'country' => $item['country'],
                'city' => $item['city'],
                'address' => $item['address'],
                'description' => $item['description'],
                'fullVegan' => $item['fullVegan'],
                'visible' => $item['visible'],
                'website' => $item['website'],
                'pictures' => $this->postrepo->findPicturesByPostId($item['id'])
            ]; 
        }
        return $likes;
    } 

    public function findByEmail(string $email): ?User
    {
        $statement = $this->connection->prepare('SELECT * FROM user WHERE email=:email');
        $statement->bindValue('email', $email);
        $statement->execute();
        $result = $statement->fetch();

        if ($result) {
            return $this->sqlToUser($result);
        }

        return null;
    }

    public function findById(int $id): ?User
    {
        $statement = $this->connection->prepare('SELECT * FROM user WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();

        if ($result) {
            return $this->sqlToUser($result);
        }

        return null;
    }

    public function persist(User $user): void
    {
        $statement = $this->connection->prepare('INSERT INTO user (name, email, password, role) VALUES (:name, :email, :password, :role)');
        $statement->execute([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'role' => $user->getRole(),
        ]);

        $user->setId($this->connection->lastInsertId());
    }

    public function delete(User $user): void
    {
        $statement = $this->connection->prepare('DELETE FROM user WHERE id=:id');
        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function update(User $user): void
    {
        $statement = $this->connection->prepare('UPDATE user SET name=:name, email=:email, password=:password, role=:role WHERE id=:id');
        $statement->execute([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'role' => $user->getRole(),
            'id' => $user->getId(),
        ]);
    }
}
