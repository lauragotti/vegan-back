<?php

namespace App\Repository;

use App\Entity\Picture;
use App\Entity\Post;
use PDO;

class PostRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }

    public function sqlToPost(array $line): Post
    {
        return new Post($line['name'], $line['country'], $line['city'], $line['address'], $line['description'], (bool) $line['fullVegan'], (bool) $line['visible'], $line['website'], (int) $line['id']);
    }
    public function sqlToPicture(array $line): Picture
    {
        return new Picture($line['src'], $line['idPost'], (int) $line['id']);
    }
    public function findAll(): array
    {
        $posts = [];
        $statement = $this->connection->prepare('SELECT * FROM post');
        $statement->execute();
        $results = $statement->fetchAll();

        foreach ($results as $item) {
            $post = $this->sqlToPost($item);
            $pictures = $this->findPicturesByPostId($post->getId());
            $post->setPictures($pictures);
            $likes = $this->findLikesByPostId($post->getId());
            $post->setLikes($likes);
            $posts[] = $post;
        }

        return $posts;
    }

    public function findLikesByPostId(int $postId): array
    {
        $likes = [];
        $statement = $this->connection->prepare('SELECT idUser FROM likes WHERE idPost=:postId');
        $statement->bindValue('postId', $postId);
        $statement->execute();
        $results = $statement->fetchAll();

        foreach ($results as $item) {
            $likes[] = $item['idUser'];
        }

        return $likes;
    }

    public function findPicturesByPostId(int $postId): array
    {
        $pictures = [];
        $statement = $this->connection->prepare('SELECT * FROM picture WHERE idPost=:postId');
        $statement->bindValue('postId', $postId);
        $statement->execute();
        $results = $statement->fetchAll();

        foreach ($results as $item) {
            $pictures[] = $this->sqlToPicture($item);
        }

        return $pictures;
    }

    public function findById(int $id): ?Post
    {
        $statement = $this->connection->prepare('SELECT * FROM post WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $result = $statement->fetch();
        if ($result) {
            $post = $this->sqlToPost($result);
            $pictures = $this->findPicturesByPostId($post->getId());
            $post->setPictures($pictures);
            $likes = $this->findLikesByPostId($post->getId());
            $post->setLikes($likes);
            return $post;
        }
        return null;
    }

    public function persist(Post $post)
    {
        $statement = $this->connection->prepare("INSERT INTO post (name, country, city, address, description, fullVegan, visible, website) VALUES (:name, :country, :city, :address, :description, :fullVegan, :visible, :website)");
        $statement->execute([
            'name' => $post->getName(),
            'country' => $post->getCountry(),
            'city' => $post->getCity(),
            'address' => $post->getAddress(),
            'description' => $post->getDescription(),
            'fullVegan' => $post->getFullVegan()?1:0,
            'visible' => $post->getVisible()?1:0,
            'website' => $post->getWebsite()
        ]);

        $post->setId($this->connection->lastInsertId());
    }

    public function delete(Post $post)
    {
        $statement = $this->connection->prepare('DELETE FROM post WHERE id=:id');
        $statement->bindValue('id', $post->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function update(Post $post): void
    {
        $statement = $this->connection->prepare('UPDATE post SET name=:name, country=:country, city=:city, address=:address, description=:description, fullVegan=:fullVegan, visible=:visible, website=:website WHERE id=:id');
        $statement->execute([
            'name' => $post->getName(),
            'country' => $post->getCountry(),
            'city' => $post->getCity(),
            'address' => $post->getAddress(),
            'description' => $post->getDescription(),
            'fullVegan' => $post->getFullVegan()?1:0,
            'visible' => $post->getVisible()?1:0,
            'website' => $post->getWebsite(),
            'id' => $post->getId()
        ]);
    }

    public function toggleLike(int $userId, int $postId): bool
    {
        $statement = $this->connection->prepare('SELECT COUNT(*) FROM likes WHERE idUser = :userId AND idPost = :postId');
        $statement->bindValue('userId', $userId);
        $statement->bindValue('postId', $postId);
        $statement->execute();
        $likeCount = (int) $statement->fetchColumn();

        if ($likeCount > 0) {
            $deleteStatement = $this->connection->prepare('DELETE FROM likes WHERE idUser = :userId AND idPost = :postId');
            $deleteStatement->bindValue('userId', $userId);
            $deleteStatement->bindValue('postId', $postId);
            $deleteStatement->execute();

            return false;
        } else {
            $insertStatement = $this->connection->prepare('INSERT INTO likes (idUser, idPost) VALUES (:userId, :postId)');
            $insertStatement->bindValue('userId', $userId);
            $insertStatement->bindValue('postId', $postId);
            $insertStatement->execute();

            return true;
        }
    }

    public function searchInCity(string $city): array {
      $query = '%' . $city . '%'; 
      $statement = $this->connection->prepare('SELECT * FROM post WHERE city LIKE :city');
      $statement->bindValue('city', $query, PDO::PARAM_STR);
      $statement->execute();
  
      $results = $statement->fetchAll();
  
      $posts = [];
  
      foreach ($results as $item) {
          $post = $this->sqlToPost($item);
          $pictures = $this->findPicturesByPostId($post->getId());
          $post->setPictures($pictures);
          $likes = $this->findLikesByPostId($post->getId());
          $post->setLikes($likes);
          $posts[] = $post;
      }
  
      return $posts;
  }
  

}