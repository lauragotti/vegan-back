<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;


class Picture {
    private ?int $id;
	#[Assert\NotBlank]
    private string $src;
	#[Assert\NotBlank]
    private int $idPost;
    public function __construct(string $src, int $idPost, ?int $id = null)
    {
        $this->id = $id;
        $this->src = $src;
        $this->idPost = $idPost;
    }

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getSrc(): string {
		return $this->src;
	}
	
	/**
	 * @param string $src 
	 * @return self
	 */
	public function setSrc(string $src): self {
		$this->src = $src;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getidPost(): int {
		return $this->idPost;
	}
	
	/**
	 * @param int $idPost 
	 * @return self
	 */
	public function setidPost(int $idPost): self {
		$this->idPost = $idPost;
		return $this;
	}
}