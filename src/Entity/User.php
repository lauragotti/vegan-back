<?php

namespace App\Entity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class User implements UserInterface, PasswordAuthenticatedUserInterface
{

    public function __construct(
		#[Assert\NotBlank]
        private ?string $name,
		#[Assert\Email]
        #[Assert\NotBlank]
        private ?string $email,
		#[Assert\Length(min: 8)]
        private ?string $password,
        private ?string $role = '', 
        private ?int $id = null
    ){}

    public function getPassword(): string {
        return $this->password;
    }

    /**
	 * @param string $password 
	 * @return self
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}
 
    public function getRoles(): array {
        return [$this->role];
	}

	public function eraseCredentials() {
	}

	public function getUserIdentifier(): string {
        return $this->email;
	}

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getName(): ?string {
		return $this->name;
	}
	
	/**
	 * @param  $name 
	 * @return self
	 */
	public function setName(?string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEmail(): ?string {
		return $this->email;
	}
	
	/**
	 * @param  $email 
	 * @return self
	 */
	public function setEmail(?string $email): self {
		$this->email = $email;
		return $this;
	}
		
    /**
	 * @return string
	 */
	public function getRole(): string {
		return $this->role;
	}
	
	/**
	 * @param  $role 
	 * @return self
	 */
	public function setRole(?string $role): self {
		$this->role = $role;
		return $this;
	}
}