<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Post {
    private ?int $id;
    #[Assert\NotBlank]
    private string $name;
    #[Assert\NotBlank]
    private string $country;
    #[Assert\NotBlank]
    private string $city;
    #[Assert\NotBlank]
    private string $address;
    private string $description;
    private bool $fullVegan;
    private bool $visible;
    private string $website;
	//to get the pictures of a post
	private array $pictures = [];
    //to get the likes
	private array $likes= [];


    public function __construct(string $name, string $country, string $city, string $address, string $description, bool $fullVegan, bool $visible, string $website, ?int $id = null){
        $this->id = $id;
        $this->name = $name;
        $this->country = $country;
        $this->city = $city;
        $this->address = $address;
        $this->description = $description;
        $this->fullVegan = $fullVegan;
        $this->visible = $visible;
        $this->website = $website;
    }

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCountry(): string {
		return $this->country;
	}
	
	/**
	 * @param string $country 
	 * @return self
	 */
	public function setCountry(string $country): self {
		$this->country = $country;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCity(): string {
		return $this->city;
	}
	
	/**
	 * @param string $city 
	 * @return self
	 */
	public function setCity(string $city): self {
		$this->city = $city;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAddress(): string {
		return $this->address;
	}
	
	/**
	 * @param string $address 
	 * @return self
	 */
	public function setAddress(string $address): self {
		$this->address = $address;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}
	
	/**
	 * @param string $description 
	 * @return self
	 */
	public function setDescription(string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getFullVegan(): bool {
		return $this->fullVegan;
	}
	
	/**
	 * @param bool $fullVegan 
	 * @return self
	 */
	public function setFullVegan(bool $fullVegan): self {
		$this->fullVegan = $fullVegan;
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function getVisible(): bool {
		return $this->visible;
	}
	
	/**
	 * @param bool $visible 
	 * @return self
	 */
	public function setVisible(bool $visible): self {
		$this->visible = $visible;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getWebsite(): string {
		return $this->website;
	}
	
	/**
	 * @param string $website 
	 * @return self
	 */
	public function setWebsite(string $website): self {
		$this->website = $website;
		return $this;
	}

	/**
     * @return array
     */
    public function getPictures(): array
    {
        return $this->pictures;
    }

    /**
     * @param array $pictures
     */
    public function setPictures(array $pictures): void
    {
        $this->pictures = $pictures;
    }

	/**
     * @return array
     */
    public function getLikes(): array
    {
        return $this->likes;
    }

    /**
     * @param array $pictures
     */
    public function setLikes(array $likes): void
    {
        $this->likes = $likes;
    }
}

